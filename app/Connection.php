<?php 

namespace App;

use PDO;

class Connection {
    private $credentials;
    private $connection;

    public function __construct(){
        $this->credentials = require(__DIR__ . '/../config/database.php');
    }

    public function getConnection(){
        // print_r($this->credentials);
        // exit;
        $dsn = $this->credentials['mysql']['name'].':'.'dbname='.$this->credentials['mysql']['dbname'].';'.'host='.$this->credentials['mysql']['host'];
        
        try{
            $this->connection = new \PDO($dsn, $this->credentials['mysql']['username'], $this->credentials['mysql']['password']);
            return $this->connection;
        }catch(\PDOException $e){
            echo "Connection Failed: ".$e->getMessage();
        }
    }
}