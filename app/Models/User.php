<?php 

namespace App\Models;

use PDO;

class User extends Model {
    protected $fields = [
        'id',
        'nama_user',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin',
        'alamat',
        'no_telp',
        'email',
        'password'
    ];

    protected $table = 'user';

    public function authenticate($email, $password) {
        $user = $this->pdo->getConnection()->query('select * from '.$this->table.' where email = "'.$email.'" and password = "'.md5($password).'"')->fetch(PDO::FETCH_OBJ);
        
        if ($user) {
            return $user;
        }

        return false;
    }
}