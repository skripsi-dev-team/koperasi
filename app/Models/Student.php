<?php 

namespace App\Models;

class Student extends Model {
    protected $fields = [
        'id',
        'name',
        'genre',
        'address'
    ];
    protected $table = 'students';
}