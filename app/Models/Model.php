<?php

namespace App\Models;

use App\Connection;
use PDO;

class Model {
    protected $fields = [];
    protected $table;
    protected $primary_key = 'id';
    protected $pdo;

    public function __construct(){
        $this->pdo = new Connection();
    }

    public function serializeData(array $data){
        $keys = [];
        foreach($data as $key => $value){
            $keys[':'.$key] = $value;
        }
        return $keys;
    }

    public function all(){
        return $this->pdo->getConnection()->query("select ".implode(',', $this->fields)." from ".$this->table)->fetchAll(PDO::FETCH_OBJ);
    }

    public function getById(int $id){
        return $this->pdo->getConnection()->query("select ".implode(',', $this->fields)." from ".$this->table." where " . $this->primary_key . " = ".$id)->fetchAll(PDO::FETCH_OBJ)[0];
    }

    public function where($field, $value){
        return $this->pdo->getConnection()->query("select ".implode(',', $this->fields)." from ".$this->table." where " . $field . " = " .$value)->fetchAll(PDO::FETCH_OBJ)[0];
    }

    public function whereAll($field, $value){
        return $this->pdo->getConnection()->query("select ".implode(',', $this->fields)." from ".$this->table." where " . $field . " = " .$value)->fetchAll(PDO::FETCH_OBJ);
    }

    public function whereAllMutasi($field, $value){
        return $this->pdo->getConnection()->query("select user.nama_user,".$this->table.".* from ".$this->table." INNER JOIN user on user.id = ".$this->table.".user_id where " . $field . " = " .$value)->fetchAll(PDO::FETCH_OBJ);
    }

    public function insert(array $data){
        array_shift($this->fields);
        $fields = implode(',', $this->fields);
        $parameters = $this->serializeData($data);
        $keys = implode(',', array_keys($parameters));

        $sql = $this->pdo->getConnection()->prepare('insert into '.$this->table.' ('.$fields.') values ('.$keys.')');

        if ($sql->execute($parameters)) {
            return $this->pdo->getConnection()->query("select * from ".$this->table. " order by ".$this->primary_key." desc limit 1")->fetch(PDO::FETCH_OBJ);
        }
        return false;
    }

    public function update(int $id, array $data){
        $statement = '';
        foreach($data as $key => $value){
            $statement .= $key."='".$value."',";
        }
        $statement = rtrim($statement, ',');
        $sql = $this->pdo->getConnection()->prepare('update '.$this->table.' set '.$statement.' where '.$this->primary_key.' = '.$id);
        $sql->execute();
        return true;
    }

    public function delete(int $id){
        return $this->pdo->getConnection()->exec('delete from '.$this->table.' where '.$this->primary_key.' = '.$id);
    }

    public function count(){
        return $this->pdo->getConnection()->query("select count(*) from ".$this->table)->fetchColumn();
    }

    public function sum($field){
        return $this->pdo->getConnection()->query("select sum(".$field.") from ".$this->table)->fetchColumn();
    }

    public function filter($column, $value) {
        return $this->pdo->getConnection()->query("select ".implode(',', $this->fields)." from pinjaman where ".$column."='".$value."'")->fetchAll(PDO::FETCH_OBJ);
    }

    public function raw(string $q) {
        return $this->pdo->getConnection()->query($q);
    }

    public function rawSingle($query){
        return $this->pdo->getConnection()->query($query)->fetchAll(PDO::FETCH_OBJ)[0];
    }
}