<?php 

namespace App\Models;

class Angsuran extends Model {
    protected $fields = [
        'id',
        'kode_angsuran',
        'tanggal_angsur',
        'jumlah',
        'bunga',
        'keterangan',
        'user_id',
        'id_pinjaman'
    ];

    protected $table = 'angsuran';

}