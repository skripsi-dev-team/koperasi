<?php 

namespace App\Models;

class Mutasi extends Model {
    protected $fields = [
        'id',
        'kode_simpanan',
        'jenis_mutasi',
        'tanggal',
        'jumlah',
        'keterangan',
        'user_id'
    ];

    protected $table = 'mutasi';

}