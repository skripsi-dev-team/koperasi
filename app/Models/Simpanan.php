<?php 

namespace App\Models;

class Simpanan extends Model {
    protected $fields = [
        'id',
        'kode_simpanan',
        'nasabah_id',
        'total_simpanan',
        'status_simpanan',
    ];

    protected $table = 'simpanan';

}