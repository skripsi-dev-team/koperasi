<?php 

namespace App\Models;

use PDO;

class Nasabah extends Model {
    protected $fields = [
        'id',
        'nama_nasabah',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin',
        'pekerjaan',
        'alamat',
        'no_telp',
        'email',
        'password'
    ];

    protected $table = 'nasabah';

    public function authenticate($email, $password) {
        $user = $this->pdo->getConnection()->query('select * from '.$this->table.' where email = "'.$email.'" and password = "'.md5($password).'"')->fetch(PDO::FETCH_OBJ);
        
        if ($user) {
            return $user;
        }

        return false;
    }

}