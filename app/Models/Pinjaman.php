<?php

namespace App\Models;

class Pinjaman extends Model {
    protected $fields = [
        'id_pinjaman',
        'kode_pinjaman',
        'nasabah_id',
        'tanggal_pinjaman',
        'jumlah_pinjam',
        'jangka_waktu',
        'jaminan',
        'tanggal_jatuh_tempo',
        'status_pinjaman',
        'bunga'
    ];

    protected $table = 'pinjaman';

    protected $primary_key = 'id_pinjaman';

}