<?php

if (!function_exists('view')) {
    function view(string $file, array $data = []) {
        global $twig;

        return $twig->render($file, $data);
    }
}

if (!function_exists('flash')) {
    function flash(string $message, string $type) {
        $_SESSION[$type][] = $message;
    }
}

if (!function_exists('getFlash')) {
    function getFlash(string $type, bool $all = false) {
        
        if (!$all) {
            return $_SESSION[$type][0];
        }

        return $_SESSION[$type];
    }
}

if (!function_exists('checkAuth')) {
    function checkAuth($type = 'admin') {
        if (isset($_SESSION['user'])) {
            if(isset($_SESSION['user_type'])){
                if($_SESSION['user_type'] == $type){
                                            
                    return $_SESSION['user'];
                }
                
            }
            return false;
        }

        return false;
    }
}

if (!function_exists('getAuthUser')){
    function getAuthUser(){
        if (checkAuth($_SESSION['user_type'])) {
                return $_SESSION['user'];
        }
    
        return null;
    }
}

if(!function_exists('base_url')) {
    function base_url(){
        return sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
           NULL
        );
    }
}

if (!function_exists('totalAngsuran')) {
    function totalAngsuran(int $pinjaman_id) {
        $angsuran = new App\Models\Angsuran;
        $angsuran_data = $angsuran->raw("select * from angsuran where id_pinjaman = '".$pinjaman_id."'")->fetchAll(PDO::FETCH_OBJ);

        $angsuran_total = 0;

        foreach ($angsuran_data as $angsuran) {
            $angsuran_total += $angsuran->jumlah;
        }

        return $angsuran_total;
    }
}

if (!function_exists('rowMutasi')){
    function rowMutasi($id) {
        $mutasi = new App\Models\Mutasi;

        return $mutasi->getById($id);
    }
}