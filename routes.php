<?php

$klein = new \Klein\Klein();

$klein->respond('GET', '/', function() {
    return view('/nasabah/login.html');
});

$klein->with('/nasabah', function() use($klein) {
    $klein->respond('POST', '/login', function($request, $response, $service) {
        $nasabah = new \App\Models\Nasabah;
        
        if ($user = $nasabah->authenticate($request->param('loginemail'), $request->param('loginpassword'))) {
            $_SESSION['user'] = $user;
            $_SESSION['user_type'] = 'nasabah';
            return $response->redirect('/nasabah/dashboard');
        }
        flash('Login gagal, pastikan email dan password sudah benar', 'error');
 
        return $service->back();
    });

    $klein->respond('GET', '/dashboard', function($request, $response) {
    
        if (checkAuth('nasabah')) {
        
            return view('nasabah/dashboard.html'); 
        }

        return $response->redirect('/');
    });

    $klein->respond('GET', '/', function($request, $response){
        if (checkAuth('nasabah')) {
            return view('nasabah/dashboard.html'); 
        }
        
        return $response->redirect('/');
    });

    $klein->respond('GET', '/simpanan', function($request,$response){
        if(checkAuth('nasabah')){
            $simpanan = new App\Models\Simpanan;
            $mutasi = new App\Models\Mutasi;
            $nasabah = getAuthUser();
            $kode_simpanan = $simpanan->where('nasabah_id', $nasabah->id)->kode_simpanan;

            $data_simpanan = $simpanan->rawSingle('SELECT sum(total_simpanan) as total_simpanan, kode_simpanan from simpanan where nasabah_id = '.$nasabah->id);
            $data_mutasi = $mutasi->whereAll('kode_simpanan', $kode_simpanan);
            
            return view('nasabah/simpanan.html', ['simpanan' => $data_simpanan, 'mutasi' => $data_mutasi]);
        }

        return $response->redirect('/');
    });

    $klein->respond('GET', '/pinjaman', function($request, $response){
        if(checkAuth('nasabah')){
            $nasabah = getAuthUser();
            $pinjaman = new App\Models\Pinjaman;
            $pinjaman_data = $pinjaman->raw("SELECT pinjaman.*, nasabah.nama_nasabah FROM pinjaman inner join nasabah on pinjaman.nasabah_id = nasabah.id where nasabah.id = ".$nasabah->id." order by tanggal_pinjaman desc")->fetchAll(PDO::FETCH_OBJ);
            
    
            return view('nasabah/pinjaman.html', ['pinjaman' => $pinjaman_data]);
            
        }

        return $response->redirect('/');
    });
    
    $klein->respond('GET', '/pinjaman/[:id]/angsuran', function($request, $response){
        $pinjaman = new App\Models\Pinjaman;
        $sql = "select pinjaman.*, nasabah.nama_nasabah, nasabah.id as nasabah_id from pinjaman inner join nasabah on pinjaman.nasabah_id = nasabah.id where pinjaman.id_pinjaman = '".$request->id."' limit 1";
        $data = $pinjaman->raw($sql)->fetch(PDO::FETCH_OBJ);

        $angsuran = new App\Models\Angsuran;
        $angsuran_data = $angsuran->raw("select * from angsuran where id_pinjaman = '".$data->id_pinjaman."'")->fetchAll(PDO::FETCH_OBJ);

        return view('nasabah/angsuran.html', ['pinjaman' => $data, 'angsuran' => $angsuran_data, 'total_angsuran' => totalAngsuran($data->id_pinjaman)]);
    });
    
});

$klein->with('/admin', function() use($klein) {
    $klein->respond('GET', '/login', function() {
        return view('admin/login.html');
    });

    $klein->respond('POST', '/login', function($request, $response, $service) {
        $admin = new \App\Models\User;
        
        if ($user = $admin->authenticate($request->param('loginemail'), $request->param('loginpassword'))) {
            $_SESSION['user'] = $user;
            $_SESSION['user_type'] = 'admin';
            return $response->redirect('/admin/dashboard');
        }

        flash('Login gagal, pastikan email dan password sudah benar', 'error');
        // print_r($service->flashes());
        return $service->back();
    });

    $klein->respond('GET', '/dashboard', function($request, $response) {
        if (checkAuth()) {
            $nasabah = new App\Models\Nasabah;
            $simpanan = new App\Models\Simpanan;
            $pinjaman = new App\Models\Pinjaman;
            $total_nasabah = $nasabah->count();
            $total_simpanan = $simpanan->sum('total_simpanan');
            $total_pinjaman = $pinjaman->sum('jumlah_pinjam');
            
            return view('admin/dashboard.html', ['total_nasabah' => $total_nasabah, 'total_simpanan' => $total_simpanan, 'total_pinjaman' => $total_pinjaman]); 
        }

        return $response->redirect('/admin/login');
    });


    /*
         =================== 
         ROUTE ADMIN NASABAH
         ===================
    */

    /* Show data nasabah */
    $klein->respond('GET', '/nasabah', function($request, $response){

        if(checkAuth()) {
            $nasabah = new App\Models\Nasabah;
            return view('admin/nasabah/index.html', ['nasabah' => $nasabah->all(), 'i' => 1]);
        }

        return $response->redirect('/admin/login');
    });
    /* End show */

    /* Detail nasabah */
    $klein->respond('POST', '/nasabah/[:id]/detail', function($request, $response){
        $nasabah = new App\Models\Nasabah;
        $detail = $nasabah->getById($request->id);

        return json_encode($detail);
    });
    /* end Detail */

     /* Form add data nasabah */
    $klein->respond('GET', '/nasabah/create', function($request, $response){
        if(checkAuth()) {
            return view('admin/nasabah/create.html');
        }
        return $response->redirect('/admin/login');
    });
    /* End Form */

    /* Store data nasabah */
    $klein->respond('POST', '/nasabah/store', function($request, $response){
        $nasabah = new App\Models\Nasabah;

        $create = $nasabah->insert([
            'nama_nasabah' => $request->param('nama_nasabah'),
            'tempat_lahir' => $request->param('tempat_lahir'),
            'tanggal_lahir' => $request->param('tanggal_lahir'),
            'jenis_kelamin' => $request->param('jenis_kelamin'),
            'pekerjaan' => $request->param('pekerjaan'),
            'alamat' => $request->param('alamat'),
            'no_telp' => $request->param('no_telp'),
            'email' => $request->param('email'),
            'password' => md5($request->param('password'))
        ]);

        $simpanan = new \App\Models\Simpanan;

        $kode_simpanan = strtotime("now").$nasabah->count();
        $simpanan->insert([
            'kode_simpanan' => $kode_simpanan,
            'nasabah_id' => $create->id,
            'total_simpanan' => 0,
            'status_simpanan' => 1
        ]);
    
        if ($create) {
            flash('Berhasil menambah data nasabah', 'success');
        }
        
        return $response->redirect('/admin/nasabah');
    });
    /* End Store */

    /* Form edit data nasabah */
    $klein->respond('GET', '/nasabah/[:id]/edit', function($request, $response){
        if(checkAuth()) {
            $nasabah = new App\Models\Nasabah;
            $data = $nasabah->getById($request->id);
            return view('admin/nasabah/edit.html', ['data' => $data]);
        }
        
        return $response->redirect('/admin/login');        
    });
    /* End form edit */

    /* Update data nasabah */
    $klein->respond('POST', '/nasabah/[:id]/update', function($request, $response){
        $nasabah = new App\Models\Nasabah;

        $update = $nasabah->update($request->id, [
            'nama_nasabah' => $request->param('nama_nasabah'),
            'tempat_lahir' => $request->param('tempat_lahir'),
            'tanggal_lahir' => $request->param('tanggal_lahir'),
            'jenis_kelamin' => $request->param('jenis_kelamin'),
            'pekerjaan' => $request->param('pekerjaan'),
            'alamat' => $request->param('alamat'),
            'no_telp' => $request->param('no_telp'),
        ]);
        if ($update) {
            flash('Berhasil mengedit data nasabah', 'success');
        }
        
        return $response->redirect('/admin/nasabah');
    });
    /* End Update */

    /* Delete data nasabah */
    $klein->respond('POST', '/nasabah/[:id]/delete', function($request, $response){
        $nasabah = new App\Models\Nasabah;

        $delete = $nasabah->delete($request->param('id'));
        if ($delete) {
            flash('Berhasil menghapus data nasabah', 'success');
        }
        
        return $response->redirect('/admin/nasabah');

    });
    /* End delete */

    /* Pinjaman Nasabah */
    $klein->respond('GET', '/pinjaman', function($request, $response) {

        $pinjaman = new App\Models\Pinjaman;
        $pinjaman_data = $pinjaman->raw("SELECT pinjaman.*, nasabah.nama_nasabah FROM pinjaman inner join nasabah on pinjaman.nasabah_id = nasabah.id order by tanggal_pinjaman desc")->fetchAll(PDO::FETCH_OBJ);
        

        if (checkAuth()) {
            return view('admin/pinjaman/index.html', ['pinjaman' => $pinjaman_data]);
        }

        return $response->redirect('/admin/login');
    });

    $klein->respond('GET', '/pinjaman/create', function($request, $response) {
        if (checkAuth()) {
            $nasabah = new App\Models\Nasabah;
            $nasabah_data = $nasabah->raw("select * from nasabah where id not in(select nasabah_id from pinjaman where status_pinjaman = 0) order by nama_nasabah")->fetchAll(PDO::FETCH_OBJ);

            return view('admin/pinjaman/create.html', ['nasabah' => $nasabah_data]);
        }

        return $response->redirect('/admin/login');
    });

    $klein->respond('POST', '/pinjaman/store', function($request, $response) {
        if (checkAuth()) {
            $pinjaman = new App\Models\Pinjaman;
            $now = Carbon\Carbon::now();
        
            $create = $pinjaman->insert([
                'kode_pinjaman' => $now->timestamp,
                'nasabah_id' => $request->param('nasabah_id'),
                'tanggal_pinjaman' => $now->format('Y-m-d'),
                'jumlah_pinjam' => $request->param('jumlah_pinjam'),
                'jangka_waktu' => $request->param('jangka_waktu'),
                'jaminan' => $request->param('jaminan'),
                'tanggal_jatuh_tempo' => $now->day,
                'status_pinjaman' => 0,
                'bunga' => $request->param('bunga')
            ]);

            if ($create) {
                flash('Pinjaman berhasil dibuat!', 'message');
            }else {
                flash('Pinjaman gagal dibuat!', 'message');
            }


            return $response->redirect('/admin/pinjaman');
        }

        return $response->redirect('/admin/login');
    });

    $klein->respond('GET', '/pinjaman/[:pinjaman_id]/data', function($request, $response) {
        if(checkAuth()){
            $pinjaman = new App\Models\Pinjaman;
            $sql = "select pinjaman.*, nasabah.nama_nasabah, nasabah.id as nasabah_id from pinjaman inner join nasabah on pinjaman.nasabah_id = nasabah.id where pinjaman.id_pinjaman = '".$request->param('pinjaman_id')."' limit 1";
            $data = $pinjaman->raw($sql)->fetch(PDO::FETCH_OBJ);
    
            $angsuran = new App\Models\Angsuran;
            $angsuran_data = $angsuran->raw("select user.nama_user, angsuran.* from angsuran inner join user on user.id = angsuran.user_id where id_pinjaman = '".$data->id_pinjaman."'")->fetchAll(PDO::FETCH_OBJ);
            
            return view('admin/pinjaman/angsuran.html', ['pinjaman' => $data, 'angsuran' => $angsuran_data, 'total_angsuran' => totalAngsuran($data->id_pinjaman)]);
        }

        return $response->redirect('/admin/login');
    });

    $klein->respond('GET', '/pinjaman/[:pinjaman_id]/angsuran/create', function($request, $response) {
        
        if (checkAuth()) {
            $pinjaman = new App\Models\Pinjaman;
            return view('admin/angsuran/create.html', ['pinjaman' => $pinjaman->getById($request->param('pinjaman_id'))]);
        }

        return $response->redirect('/admin/login');
    });

    $klein->respond('POST', '/pinjaman/[:pinjaman_id]/angsuran/store', function($request, $response) {
        if (checkAuth()) {
            $angsuran = new App\Models\Angsuran;
            $now = Carbon\Carbon::now();

            $pinjaman = new App\Models\Pinjaman;
            $pinjaman_data = $pinjaman->getById($request->param('pinjaman_id'));

            $sisa_hutang = $pinjaman_data->jumlah_pinjam - totalAngsuran($request->pinjaman_id);
            
            if($sisa_hutang < $request->param('jumlah_angsuran')){
                flash('Gagal menambah angsuran karena jumlah angsuran melebihi jumlah pinjaman!', 'error');
                return $response->redirect('/admin/pinjaman/'.$request->param('pinjaman_id').'/data');
            }
            

            $remain = $pinjaman_data->jumlah_pinjam - totalAngsuran($pinjaman_data->id_pinjaman);

            if ($request->param('jumlah_angsuran') >= $remain) {
                $bunga = 0;
            }else {
                
                $bunga =  $pinjaman_data->bunga / 100;
    
                if ($now->day > $pinjaman_data->tanggal_jatuh_tempo) {
                    $penalty = (0.5 * ($now->day - $pinjaman_data->tanggal_jatuh_tempo));
                    $bunga = ($bunga + ($penalty / 100));
                }

                
                $bunga = ($remain * $bunga);
            }

            $create = $angsuran->insert([
                'kode_angsuran' => $now->timestamp . '_' . $request->param('pinjaman_id'),
                'tanggal_angsur' => $now->format('Y-m-d'),
                'jumlah'    => $request->param('jumlah_angsuran') - $bunga,
                'bunga' => $bunga,
                'keterangan' => $request->param('keterangan'),
                'user_id' => getAuthUser()->id,
                'id_pinjaman' => $request->param('pinjaman_id')
            ]);

            $remain = $remain - $create->jumlah;
            if ($remain <= 0) {
                $pinjaman->update($pinjaman_data->id_pinjaman, ['status_pinjaman' => 1]);
            }

            if ($create) {
                # code...
                flash('Angsuran berhasil disimpan!', 'success');
            } else {
                flash('Angsuran gagal disimpan!', 'error');
                # code...
            }
            

            return $response->redirect('/admin/pinjaman/'.$request->param('pinjaman_id').'/data');
        }

        return $response->redirect('/admin/login');
    });


    /*
         =================== 
         ROUTE ADMIN USER
         ===================
    */

    /* Show data user */
    $klein->respond('GET', '/user', function($request, $response){

        if(checkAuth()) {
            $users = new App\Models\User;
            return view('admin/user/index.html', ['users' => $users->all()]);
        }

        return $response->redirect('/admin/login');
    });
    /* End show */

    /* Detail user */
    $klein->respond('POST', '/user/[:id]/detail', function($request, $response){
        $user = new App\Models\User;
        $detail = $user->getById($request->id);

        return json_encode($detail);
    });
    /* end Detail */

    /* Form add data user */
    $klein->respond('GET', '/user/create', function($request, $response){
        if(checkAuth()) {
            return view('admin/user/create.html');
        }
        return $response->redirect('/admin/login');
    });
    /* End Form */

    /* Store data user */
    $klein->respond('POST', '/user/store', function($request, $response){
        $nasabah = new App\Models\User;
        
        $create = $nasabah->insert([
            'nama_user' => $request->param('nama_user'),
            'tempat_lahir' => $request->param('tempat_lahir'),
            'tanggal_lahir' => $request->param('tanggal_lahir'),
            'jenis_kelamin' => $request->param('jenis_kelamin'),
            'alamat' => $request->param('alamat'),
            'no_telp' => $request->param('no_telp'),
            'email' => $request->param('email'),
            'password' => md5($request->param('password'))
        ]);

        if ($create) {
            flash('Berhasil menambah data petugas', 'success');
        }
        
        return $response->redirect('/admin/user');
    });
    /* End Store */

    /* Form edit data user */
    $klein->respond('GET', '/user/[:id]/edit', function($request, $response){
        if(checkAuth()) {
            $user = new App\Models\User;
            $data = $user->getById($request->id);
            return view('admin/user/edit.html', ['data' => $data]);
        }
        
        return $response->redirect('/admin/login');        
    });
    /* End form edit */

    /* Update data petugas */
    $klein->respond('POST', '/user/[:id]/update', function($request, $response){
        $nasabah = new App\Models\User;

        $update = $nasabah->update($request->id, [
            'nama_user' => $request->param('nama_user'),
            'tempat_lahir' => $request->param('tempat_lahir'),
            'tanggal_lahir' => $request->param('tanggal_lahir'),
            'jenis_kelamin' => $request->param('jenis_kelamin'),
            'alamat' => $request->param('alamat'),
            'no_telp' => $request->param('no_telp'),
        ]);
        if ($update) {
            flash('Berhasil mengedit data petugas', 'success');
        }
        
        return $response->redirect('/admin/user');
    });
    /* End Update */


    /* Delete data user */
    $klein->respond('POST', '/user/[:id]/delete', function($request, $response){
        $user = new App\Models\User;

        $petugas = getAuthUser()->id;
        if($petugas == $request->id){
            flash('Tidak dapat menghapus data anda sendiri', 'error');
            return $response->redirect('/admin/user');
        }
        
        $delete = $user->delete($request->param('id'));
        if ($delete) {
            flash('Berhasil menghapus data petugas', 'success');
        }
        
        return $response->redirect('/admin/user');

    });
    /* End delete */

    /*
         =================== 
         ROUTE SIMPANAN
         ===================
    */

    /* Show data user */
    $klein->respond('GET', '/simpanan', function($request, $response){

        if(checkAuth()) {
            $simpanan = new App\Models\Simpanan;
            $data = $simpanan->raw("SELECT * FROM simpanan INNER JOIN nasabah on simpanan.nasabah_id = nasabah.id ")->fetchAll(PDO::FETCH_OBJ);
            $total = $total_simpanan = $simpanan->sum('total_simpanan');

            return view('admin/simpanan/index.html', ['simpanan' => $data, 'total' => $total]);
        }

        return $response->redirect('/admin/login');
    });
    /* End show */


    /* form add simpanan (mutasi) */
    $klein->respond('GET', '/simpanan/[:kode_simpanan]/create', function($request, $response, $kode_simpanan){
        if(checkAuth()) {
            $simpanan = new App\Models\Simpanan;
            $data = $simpanan->rawSingle("SELECT * FROM simpanan INNER JOIN nasabah on simpanan.nasabah_id = nasabah.id WHERE kode_simpanan = ".$request->kode_simpanan);
            if($data->status_simpanan == 0){
                flash('Tidak dapat menambah transaksi simpanan yang tidak aktif', 'error');
                return $response->redirect('/admin/simpanan');
            }
            return view('admin/simpanan/create.html', ['nasabah' => $data]);
        }
        return $response->redirect('/admin/login');
    });
    /* End show form */

    /* Simpan Mutasi */
    $klein->respond('POST', '/simpanan/mutasi', function($request, $response){
        $simpanan = new \App\Models\Simpanan;
        $mutasi = new \App\Models\Mutasi;
        $petugas = getAuthUser()->id;

        $data_simpanan = $simpanan->where('kode_simpanan', $request->param('kode_simpanan'));
    
        if($request->param('jenis_mutasi') == 1){
            //simpanan
            $saldo = $data_simpanan->total_simpanan + $request->param('jumlah');
            
        } else {
            //penarikan
            if($data_simpanan->total_simpanan < $request->param('jumlah')){
                flash('Gagal melakukan penarikan karena saldo tidak mencukupi.', 'error');
                return $response->redirect('/admin/simpanan/'.$request->param('kode_simpanan')."/create");
            }
            $saldo = $data_simpanan->total_simpanan - $request->param('jumlah');
        }

        $mutasi->insert([
            'kode_simpanan' => $request->param('kode_simpanan'),
            'jenis_mutasi' => $request->param('jenis_mutasi'),
            'tanggal' => $request->param('tanggal'),
            'jumlah' => $request->param('jumlah'),
            'keterangan' => $request->param('keterangan'),
            'user_id' => $petugas
        ]);

        $simpanan->update($data_simpanan->id, [
            'total_simpanan' => $saldo
        ]);

        flash('Berhasil melakukan transaksi.', 'success');
        return $response->redirect('/admin/simpanan');
    });

    /** end mutasi */

    /** ubah status simpanan */
    $klein->respond('POST', '/simpanan/update', function($request, $response){
        $simpanan = new \App\Models\Simpanan;

        $data_simpanan = $simpanan->where('kode_simpanan', $request->param('kode_simpanan'));

        $simpanan->update($data_simpanan->id, [
            'status_simpanan' => $request->param('status_simpanan')
        ]);

        flash('Berhasil merubah status simpanan.', 'success');
        return $response->redirect('/admin/simpanan');

    });

    /** end ubah status */

    /** Detail mutasi */
    $klein->respond('GET', '/simpanan/[:kode_simpanan]/mutasi', function($request, $response){
        $simpanan = new App\Models\Simpanan;
        $mutasi = new App\Models\Mutasi;

        $data_simpanan = $simpanan->rawSingle("SELECT * FROM simpanan INNER JOIN nasabah on simpanan.nasabah_id = nasabah.id WHERE kode_simpanan = ".$request->kode_simpanan);
        $data_mutasi = $mutasi->whereAllMutasi('kode_simpanan', $request->kode_simpanan);

        return view('admin/simpanan/mutasi.html', ['simpanan' => $data_simpanan, 'mutasi' => $data_mutasi]);
    });

    /** end detail mutasi */



    $klein->respond('GET', '/simpanan/[:kode_mutasi]/mutasi/print', function($request, $response){
        $dompdf = new Dompdf\Dompdf;
        $mutasi = new App\Models\Mutasi;
        
        $mutasi_id = $_GET['id'];

        $data = $mutasi->rawSingle("SELECT * FROM mutasi INNER JOIN simpanan ON simpanan.kode_simpanan = mutasi.kode_simpanan INNER JOIN nasabah ON nasabah.id = simpanan.nasabah_id WHERE mutasi.id = '$mutasi_id'");
        if($data->jenis_mutasi == 1) {
            $jenis_mutasi = "Setoran";
        } else {
            $jenis_mutasi = "Penarikan";
        }
        

        $file = base_url().'/views/admin/simpanan/print_mutasi.php?kode_simpanan='.$data->kode_simpanan.'&nama_nasabah='.$data->nama_nasabah.'&jumlah='.$data->jumlah.'&tanggal='.$data->tanggal.'&jenis_mutasi='.$jenis_mutasi.'&keterangan='.$data->keterangan.'&petugas='.getAuthUser()->nama_user;

        $file = str_replace(" ","%", $file);

        $content = file_get_contents($file);

        $dompdf->loadHtml($content);


        $dompdf->render();
        // Output the generated PDF to Browser
        $dompdf->stream("mutasi-harian-nasabah.pdf");
    });

    $klein->respond('GET', '/pinjaman/[:id_pinjaman]/data/print', function($request, $response){
        $dompdf = new Dompdf\Dompdf;
        $angsuran = new App\Models\Angsuran;
        
        $angsuran_id = $_GET['id'];

        $data = $angsuran->rawSingle("SELECT * FROM angsuran INNER JOIN pinjaman ON pinjaman.id_pinjaman = angsuran.id_pinjaman INNER JOIN nasabah ON nasabah.id = pinjaman.nasabah_id WHERE angsuran.id = '$angsuran_id'");

        $file = base_url().'/views/admin/pinjaman/print_angsuran.php?kode_angsuran='.$data->kode_angsuran.'&nama_nasabah='.$data->nama_nasabah."&jumlah_angsuran=".$data->jumlah."&bunga=".$data->bunga."&tanggal_angsur=".$data->tanggal_angsur."&keterangan=".$data->keterangan.'&petugas='.getAuthUser()->nama_user;

        $file = str_replace(" ","%", $file);
        
        $content = file_get_contents($file);


        $dompdf->loadHtml($content);

        $dompdf->render();

        $dompdf->stream("angsuran-harian-nasabah.pdf");

    });

    $klein->respond('GET', '/laporan-simpan', function($request, $response){
        if(checkAuth()) {
            $mutasi = new App\Models\Mutasi;  
            $parse['url'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 
         
            if(isset($_GET['cari'])){
                $parse['mutasi'] = $mutasi->raw("SELECT * FROM mutasi INNER JOIN simpanan ON simpanan.kode_simpanan = mutasi.kode_simpanan INNER JOIN nasabah ON nasabah.id = simpanan.nasabah_id WHERE tanggal BETWEEN '". $_GET['dari'] ."' AND '". $_GET['sampai']."'")->fetchAll();
                $parse['type'] = 1;
                $parse['dari'] = $_GET['dari'];
                $parse['sampai'] = $_GET['sampai'];   
                
                if(isset($_GET['print'])){
                    $parse['dateNow'] = date('Y-m-d');
                    $parse['petugas'] = getAuthUser()->nama_user;
                    $dompdf = new Dompdf\Dompdf;
                    $dompdf->loadHtml(view('admin/laporan/print_laporan_simpanan.php', $parse));
                    $dompdf->render();
                    $dompdf->stream('LaporanSimpanan.pdf');
                }

            } else {
                $parse['type'] = 0;
                       
            }

            return view('admin/laporan/simpanan.php', $parse); 

        
        }
        return $response->redirect('/admin/login');
    });

    $klein->respond('GET', '/laporan-pinjam', function($request, $response){
        if(checkAuth()) {
            $pinjaman = new App\Models\Pinjaman;   
            $parse['url'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

            if(isset($_GET['cari'])){
                $parse['pinjaman'] = $pinjaman->raw("SELECT * FROM pinjaman INNER JOIN nasabah ON nasabah.id = pinjaman.nasabah_id WHERE tanggal_pinjaman BETWEEN '". $_GET['dari'] ."' AND '". $_GET['sampai']."'")->fetchAll();
                $parse['type'] = 1;
                $parse['dari'] = $_GET['dari'];
                $parse['sampai'] = $_GET['sampai'];        
                if(isset($_GET['print'])){
                    $parse['dateNow'] = date('Y-m-d');
                    $parse['petugas'] = getAuthUser()->nama_user;
                    $dompdf = new Dompdf\Dompdf;
                    $dompdf->loadHtml(view('admin/laporan/print_laporan_pinjaman.php', $parse));
                    $dompdf->render();
                    $dompdf->stream('LaporanPinjaman.pdf');
                }    

            } else {
                $parse['type'] = 0;
                       
            }

            return view('admin/laporan/pinjaman.php', $parse); 

        
        }
        return $response->redirect('/admin/login');
    });

    $klein->respond('GET', '/laporan-angsur', function($request, $response){
        if(checkAuth()) {
            $angsuran = new App\Models\Angsuran;   
            $parse['url'] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
         
            if(isset($_GET['cari'])){
                $parse['angsuran'] = $angsuran->raw("SELECT * FROM angsuran INNER JOIN pinjaman ON pinjaman.id_pinjaman = angsuran.id_pinjaman INNER JOIN nasabah ON nasabah.id = pinjaman.nasabah_id INNER JOIN user on user.id = angsuran.user_id WHERE tanggal_angsur BETWEEN '". $_GET['dari'] ."' AND '". $_GET['sampai']."'")->fetchAll();
                $parse['type'] = 1;
                $parse['dari'] = $_GET['dari'];
                $parse['sampai'] = $_GET['sampai'];  

                if(isset($_GET['print'])){
                    $parse['dateNow'] = date('Y-m-d');
                    $parse['petugas'] = getAuthUser()->nama_user;
                    $dompdf = new Dompdf\Dompdf;
                    $dompdf->loadHtml(view('admin/laporan/print_laporan_angsuran.php', $parse));
                    $dompdf->render();
                    $dompdf->stream('LaporanAngsuran.pdf');
                }             

            } else {
                $parse['type'] = 0;
                       
            }

            return view('admin/laporan/angsuran.php', $parse); 

        
        }
        return $response->redirect('/admin/login');
    });



});

$klein->respond('GET', '/logout', function($request, $response) {
    unset($_SESSION['user']);

    if ($_SESSION['user_type'] == 'nasabah') {
        unset($_SESSION['user_type']);
        return $response->redirect('/');
    }

    unset($_SESSION['user_type']);
    $response->redirect('/admin/login');
});




$klein->dispatch();