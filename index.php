<?php
session_start();

require __DIR__ . "/vendor/autoload.php";

require __DIR__ . "/bootstrap/bootstrap.php";

require "routes.php";