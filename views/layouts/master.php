<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ base_url() }}/public/templates/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{ base_url() }}/public/templates/vendor/font-awesome/css/font-awesome.min.css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{ base_url() }}/public/templates/css/fontastic.css">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ base_url() }}/public/templates/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ base_url() }}/public/templates/css/custom.css">

    <link rel="stylesheet" href="{{ base_url() }}/public/assets/select2/css/select2.min.css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
    <!-- DATEPICKER -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
        .select2 .selection {
            width: 100%;
        }
    </style>

    
    <title>{% block head %} {% endblock %} - Koperasi Mertha Sentana</title>
    
</head>
<body>
    <div class="page">
        {% if(getAuthUser() == null) %}
            <!-- MAIN CONTENT -->
            {% block content_login %}
                
            {% endblock %}
        
        {% else %}
        
        {{ include('layouts/header.php') }}
        <div class="page-content d-flex align-items-stretch"> 
            {{ include('layouts/sidebar.php') }}   

            
            <div class="content-inner" style="padding-bottom: 59px;">
                <header class="page-header">
                    <div class="container-fluid">
                        <h2 class="no-margin-bottom">{% block title %} {% endblock %}</h2>
                    </div>
                </header>

                {% if(checkFlash('success')) %}
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success">{{ getFlash('success') }}</div>
                    </div>
                </div>
                {% endif %}

                {% if(checkFlash('error')) %}
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger">{{ getFlash('error') }}</div>
                    </div>
                </div>
                {% endif %}
                <!-- MAIN CONTENT -->
                {% block content %}
                
                {% endblock %}
                
                {{ include('layouts/footer.php') }}  
            
            </div>                      
        </div>
        {% endif %}
    </div>    
    <!-- JavaScript files-->
    <script src="{{ base_url() }}/public/templates/vendor/jquery/jquery.min.js"></script>
    <script src="{{ base_url() }}/public/templates/vendor/popper.js/umd/popper.min.js"> </script>
    <script src="{{ base_url() }}/public/templates/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ base_url() }}/public/templates/vendor/bootstrap/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ base_url() }}/public/templates/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="{{ base_url() }}/public/templates/vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="{{ base_url() }}/public/assets/select2/js/select2.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>

    <!-- Main File-->
    <script src="{{ base_url() }}/public/templates/js/front.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>

    


    <script>
        $(document).ready(function() {
            var rows = document.getElementById('my-table').getElementsByTagName("tr").length - 1;
            
            $('#my-table').DataTable({
                dom: 'Bfrtip',
                buttons: [
                   
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    },
                    {
                        extend: 'pdfHtml5',                        
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                    }
                ]
            });
            
            
            $('.datepicker').datepicker({
                dateFormat: 'yy-mm-dd',
                changeYear: true,
                changeMonth: true,
                yearRange: '1950:2019'
            });

            $('.select2').select2({
                width: '100%',
                placeholder: 'Select...'
            });

        });

        $(document).ready(function(){
            $('#laporan-table').DataTable();
        });
    </script>

    {% block scripts %}

    {% endblock %}
</body>
</html>