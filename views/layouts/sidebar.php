<!-- Side Navbar -->
<nav class="side-navbar my-navbar">
        <!-- Sidebar Header-->
        <div class="sidebar-header d-flex align-items-center">
          <div class="avatar"><img src="{{ base_url() }}/public/templates/img/profile.png" alt="profile" class="img-fluid rounded-circle"></div>
          <div class="title">
            {% if(getUserType() == 'admin') %}
              <h1 class="h4">Halo, {{ getAuthUser().nama_user }}</h1>
              <p>Selamat Bekerja</p>
            {% else %}
              <h1 class="h4">Halo, {{ getAuthUser().nama_nasabah }}</h1>
              <p>Selamat Bekerja</p>
            {% endif %}
          </div>
        </div>
        <!-- Sidebar Navidation Menus-->
        <span class="heading">Master</span>
        <ul class="list-unstyled">
          <li class="{{ checkSidebar('dashboard') ? 'active':'' }}"><a href="/admin/dashboard"> <i class="icon-home"></i>Home </a></li>
          <li class="{{ checkSidebar('nasabah') ? 'active':'' }}"><a href="/admin/nasabah"> <i class="fa fa-users"></i>Nasabah </a></li>
          <li class="{{ checkSidebar('user') ? 'active':'' }}"><a href="/admin/user"> <i class="icon-user"></i>Petugas</a></li>          
        </ul>
        
        <span class="heading">TRANSAKSI</span>
        <ul class="list-unstyled">
          <li class="{{ checkSidebar('simpanan') ? 'active':'' }}"><a href="/admin/simpanan"> <i class="fa fa-book"></i>Simpanan</a></li>
          <li class="{{ checkSidebar('pinjaman') ? 'active':'' }}"> <a href="/admin/pinjaman"> <i class="fa fa-file"></i>Pinjaman </a></li>          
        </ul>

     
        <ul class="list-unstyled">
          <li><a href="#exampledropdownDropdown" aria-expanded="false" data-toggle="collapse" class="collapsed"> <i class="icon-interface-windows"></i>Laporan </a>
            <ul id="exampledropdownDropdown" class="list-unstyled collapse {{ checkSidebar('laporan') ? 'show':'' }}" style="">
              <li class="{{ checkSidebar('laporan-simpan') ? 'active':'' }}"><a href="/admin/laporan-simpan">Simpanan</a></li>
              <li class="{{ checkSidebar('laporan-pinjam') ? 'active':'' }}"><a href="/admin/laporan-pinjam">Pinjaman</a></li>
              <li class="{{ checkSidebar('laporan-angsur') ? 'active':'' }}"><a href="/admin/laporan-angsur">Angsuran</a></li>
            </ul>
          </li>
        </ul>
<!--  -->
      </nav>
     