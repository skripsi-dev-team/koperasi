{% extends "layouts/master.php" %}

{% block head %}
Laporan Simpanan
{% endblock %}

{% block title %}
Laporan Simpanan
{% endblock %}

{% block content %}

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="h4">Tampilkan Laporan Simpanan</h3>
                    </div>
                    <div class="card-body">
                        <form action="/admin/laporan-simpan" method="get">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Dari tanggal</label>
                                    <input type="date" class="form-control" name="dari" required value="{{ dari }}">
                                </div>
                                <div class="col-md-6">
                                    <label>Sampai tanggal</label>
                                    <input type="date" class="form-control" name="sampai" required value="{{ sampai }}">
                                </div>
                            </div>
                            <br>
                            <input type="submit" value="Cari Data" name="cari" class="btn btn-outline-success float-right">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {% if type == 1 %}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Laporan dari tanggal {{ dari }} sampai {{ sampai }} </h4>
                    </div>
                    <div class="card-body">
                        <a href="{{ url }}&print=true" class="btn btn-sm btn-info">Print</a>
                        <br><br>
                        <table class="table" id="laporan-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>No Simpanan</th>
                                    <th>Nama Nasabah</th>
                                    <th>Tanggal</th>
                                    <th>Jumlah</th>
                                    <th>Jenis Mutasi</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>

                                {% for data in mutasi %}
                                {% set number = ( number | default(0) ) + 1 %}

                                <tr>
                                    <td>{{ number }}</td>
                                    <td>{{ data.kode_simpanan }}</td>
                                    <td>{{ data.nama_nasabah }}</td>
                                    <td>{{ data.tanggal }}</td>
                                    <td>{{ convertNumber(data.jumlah) }}</td>
                                    <td>{{ getJenis(data.jenis_mutasi) }}</td>
                                    <td>{{ data.keterangan }}</td>
                                    <td>OK</td>                                                                        
                                </tr>

                                {% endfor %}
                            </tbody>
                        </table>   
                    </div>
                </div>
            </div>
        </div>
        {% endif %}
    </div>
 
</section>

{% endblock %}