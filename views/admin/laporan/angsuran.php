{% extends "layouts/master.php" %}

{% block head %}
Laporan Angsuran
{% endblock %}

{% block title %}
Laporan Simpanan
{% endblock %}

{% block content %}

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="h4">Tampilkan Laporan Angsuran</h3>
                    </div>
                    <div class="card-body">
                        <form action="/admin/laporan-angsur" method="get">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Dari tanggal</label>
                                    <input type="date" class="form-control" name="dari" required value="{{ dari }}">
                                </div>
                                <div class="col-md-6">
                                    <label>Sampai tanggal</label>
                                    <input type="date" class="form-control" name="sampai" required value="{{ sampai }}">
                                </div>
                            </div>
                            <br>
                            <input type="submit" value="Cari Data" name="cari" class="btn btn-outline-success float-right">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {% if type == 1 %}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Laporan dari tanggal {{ dari }} sampai {{ sampai }} </h4>
                    </div>
                    <div class="card-body">
                        <a href="{{ url }}&print=true" class="btn btn-sm btn-info">Print</a>
                        <br><br>
                        <table class="table" id="laporan-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Angsuran</th>
                                    <th>Nama Nasabah</th>
                                    <th>Tanggal</th>
                                    <th>Bunga</th>
                                    <th>Bayar</th>
                                    <th>Petugas</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                {% for data in angsuran %}
                                    {% set number = ( number | default(0) ) + 1 %}
                                    <tr>
                                        <td>{{ number }}</td>
                                        <td>{{ data.kode_angsuran }}</td>
                                        <td>{{ data.nama_nasabah }}</td>
                                        <td>{{ data.tanggal_angsur }}</td>
                                        <td>{{ data.bunga }}%</td>
                                        <td>{{ convertNumber(data.jumlah) }}</td>
                                        <td>{{ data.nama_user }}</td>
                                        <td>OK</td>
                                    </tr>
                                {% endfor %}
                            </tbody>
                        </table>   
                    </div>
                </div>
            </div>
        </div>
        {% endif %}
    </div>
 
</section>

{% endblock %}