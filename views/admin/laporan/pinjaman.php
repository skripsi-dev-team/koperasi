{% extends "layouts/master.php" %}

{% block head %}
Laporan Simpanan
{% endblock %}

{% block title %}
Laporan Pinjaman
{% endblock %}

{% block content %}

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="h4">Tampilkan Laporan Pinjaman</h3>
                    </div>
                    <div class="card-body">
                        <form action="/admin/laporan-pinjam" method="get">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Dari tanggal</label>
                                    <input type="date" class="form-control" name="dari" required value="{{ dari }}">
                                </div>
                                <div class="col-md-6">
                                    <label>Sampai tanggal</label>
                                    <input type="date" class="form-control" name="sampai" required value="{{ sampai }}">
                                </div>
                            </div>
                            <br>
                            <input type="submit" value="Cari Data" name="cari" class="btn btn-outline-success float-right">
                        </form>
                    </div>
                </div>
            </div>
        </div>

        {% if type == 1 %}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Laporan dari tanggal {{ dari }} sampai {{ sampai }} </h4>
                    </div>
                    <div class="card-body">
                        <a href="{{ url }}&print=true" class="btn btn-sm btn-info">Print</a>
                        <br><br>
                        <table class="table" id="laporan-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Nasabah</th>
                                    <th>Kode Pinjaman</th>
                                    <th>Jumlah Pinjaman</th>
                                    <th>Tanggal Pinjaman</th>
                                    <th>Status</th>
                                    <th>Hasil</th>
                                </tr>
                            </thead>
                            <tbody>
                                {% for data in pinjaman %}
                                {% set number = ( number | default(0) ) + 1 %}
                                <tr>
                                    <td>{{ number }}</td>
                                    <td>{{ data.nama_nasabah }}</td>
                                    <td>{{ data.kode_pinjaman }}</td>
                                    <td>{{ convertNumber(data.jumlah_pinjam) }}</td>
                                    <td>{{ data.tanggal_pinjaman }}</td>
                                    <td>{{ getStatusPinjaman(data.status_pinjaman)|raw }}</td>
                                    <td>OK</td>
                                </tr>
                                {% endfor %}
                            </tbody>
                        </table>   
                    </div>
                </div>
            </div>
        </div>
        {% endif %}
    </div>
 
</section>

{% endblock %}