<style> 
table {
  border-collapse: collapse;
  width:100%;
}

table, th, td {
  border: 1px solid black;
}

.admin {
    display: inline-block;
    width:49%;
}

.ketua {
    display: inline-block;
    float: right;
    text-align: right;
    width:49%;
}
.logo {
    text-align:center;
}
img {
    width:80px;
}
.header {
    text-align: center;
    line-height: 1em;
}
</style>
<div class="header">
    <h3>Koperasi Mertha Sentana</h3>
    <p>Br. Batanwani, Kukuh, Marga, Tabanan</p>
    <p>Phone: +62 819 17097901</p>
    <hr>
</div>
<p>Laporan simpanan dari tanggal {{ dari }} sampai {{ sampai }} </p>
<table class="table" id="laporan-table">
    <thead>
        <tr>
            <th>No</th>
            <th>No Simpanan</th>
            <th>Nama Nasabah</th>
            <th>Tanggal</th>
            <th>Jumlah</th>
            <th>Jenis Mutasi</th>
            <th>Keterangan</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>

        {% for data in mutasi %}
        {% set number = ( number | default(0) ) + 1 %}

        <tr>
            <td>{{ number }}</td>
            <td>{{ data.kode_simpanan }}</td>
            <td>{{ data.nama_nasabah }}</td>
            <td>{{ data.tanggal }}</td>
            <td>{{ convertNumber(data.jumlah) }}</td>
            <td>{{ getJenis(data.jenis_mutasi) }}</td>
            <td>{{ data.keterangan }}</td>
            <td>OK</td>                                                                        
        </tr>

        {% endfor %}
    </tbody>
</table>


<br><br>
<p><strong>Tabanan, {{ dateNow }}</strong></p>
<div class="ttd">
    <div class="admin">
        Petugas
        <br><br><br><br>
        {{ petugas }}
    </div>
    <div class="ketua">
        Kepala Koperasi
        <br><br><br><br>
        I Ketut Suarya
    </div>
</div>