<style> 
table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
}

.admin {
    display: inline-block;
    width:49%;
}

.ketua {
    display: inline-block;
    float: right;
    text-align: right;
    width:49%;
}
.logo {
    text-align:center;
}
img {
    width:80px;
}
.header {
    text-align: center;
    line-height: 1em;
}
</style>
<div class="logo">

    <!-- <img src="../../../public/logo-koperasi.jpeg"> -->
    
</div>
<div class="header">
    <h3>Koperasi Mertha Sentana</h3>
    <p>Br. Batanwani, Kukuh, Marga, Tabanan</p>
    <p>Phone: +62 819 17097901</p>
    <hr>
</div>
<p>Mutasi Harian Nasabah Tanggal <strong><?= $_GET['tanggal'] ?></strong></p>
<table border="1" width="100%">
    <tr>
        <td>No Nasabah</td>
        <td><?= $_GET['kode_simpanan'] ?></td>
    </tr>
    <tr>
        <td>Nama Nasabah</td>
        <td><?= str_replace("%", " ", $_GET['nama_nasabah']) ?></td>
    </tr>
    <tr>
        <td>Jenis Mutasi</td>
        <td><?= $_GET['jenis_mutasi'] ?></td>
    </tr>
    <tr>
        <td>Jumlah</td>
        <td>RP. <?= number_format($_GET['jumlah']) ?></td>
    </tr>
    <tr>
        <td>Keterangan</td>
        <td><?= $_GET['keterangan'] ?></td>
    </tr>
</table>

<br><br>
<p><strong>Tabanan, <?= date('Y-m-d') ?></strong></p>
<div class="ttd">
    <div class="admin">
        Petugas
        <br><br><br><br>
        <?= str_replace("%", " ",$_GET['petugas']) ?>
    </div>
    <div class="ketua">
        Kepala Koperasi
        <br><br><br><br>
        I Ketut Suarya
    </div>
</div>

