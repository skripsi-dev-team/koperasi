<style> 
table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
}

.admin {
    display: inline-block;
    width:49%;
}

.ketua {
    display: inline-block;
    float: right;
    text-align: right;
    width:49%;
}
.logo {
    text-align:center;
}
img {
    width:80px;
}
.header {
    text-align: center;
    line-height: 1em;
}
</style>
<div class="header">
    <h3>Koperasi Mertha Sentana</h3>
    <p>Br. Batanwani, Kukuh, Marga, Tabanan</p>
    <p>Phone: +62 819 17097901</p>
    <hr>
</div>
<p>Angsuran Harian Nasabah Tanggal <strong><?= $_GET['tanggal_angsur'] ?></strong></p>
<table border="1" width="100%">
    <tr>
        <td>No Angsuran</td>
        <td><?= $_GET['kode_angsuran'] ?></td>
    </tr>
    <tr>
        <td>Nama Nasabah</td>
        <td><?= str_replace("%", " ", $_GET['nama_nasabah']) ?></td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td><?= $_GET['tanggal_angsur'] ?></td>
    </tr>
    <tr>
        <td>Jumlah</td>
        <td>RP. <?= number_format($_GET['jumlah_angsuran']) ?></td>
    </tr>
    <tr>
        <td>Bunga</td>
        <td><?= $_GET['bunga'] ?>%</td>
    </tr>
    <tr>
        <td>Keterangan</td>
        <td><?= $_GET['keterangan'] ?></td>
    </tr>
    
</table>


<br><br>
<p><strong>Tabanan, <?= date('Y-m-d') ?></strong></p>
<div class="ttd">
    <div class="admin">
        Petugas
        <br><br><br><br>
        <?= str_replace("%", " ",$_GET['petugas']) ?>
    </div>
    <div class="ketua">
        Kepala Koperasi
        <br><br><br><br>
        I Ketut Suarya
    </div>
</div>