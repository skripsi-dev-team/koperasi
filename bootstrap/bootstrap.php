<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$basePath = dirname(__DIR__);


require $basePath . "/app/helpers.php";

$loader = new \Twig\Loader\FilesystemLoader($basePath . '/views');
$twig = new \Twig\Environment($loader, [
    'cache' => $basePath . '/views/cache',
    'auto_reload' => true
]);

$get_flash = new \Twig\TwigFunction('getFlash', function (string $type, bool $all = false) {
    $messages = null;
    if (!$all) {
        if (isset($_SESSION[$type])) {
            $messages = $_SESSION[$type][0];
            $_SESSION[$type] = null;
        }

        return $messages;
    }

    if (isset($_SESSION[$type])) {
        $messages = $_SESSION[$type];
        $_SESSION[$type] = null;
    }

    return $messages;
});

$check_flash = new \Twig\TwigFunction('checkFlash', function (string $type) {
    if (isset($_SESSION[$type])) {
        return true;
    }

    return false;
});

$check_auth = new \Twig\TwigFunction('checkAuth', function () {
    if (isset($_SESSION['user'])) {
        return true;
    }

    return false;
});

$get_auth_user = new \Twig\TwigFunction('getAuthUser', function() {
    if(isset($_SESSION['user_type'])){
        if (checkAuth($_SESSION['user_type'])) {
            return $_SESSION['user'];
        }
    }

    return null;
});

$get_user_type = new \Twig\TwigFunction('getUserType', function() {
    if (checkAuth()) {
        return $_SESSION['user_type'];
    }

    return false;
});

$get_jk = new \Twig\TwigFunction('getJK', function($jk) {
    if($jk == 'l'){
        return "Pria";
    } else {
        return "Wanita";
    }
});

$base_url = new \Twig\TwigFunction('base_url', function(){
    return sprintf(
        "%s://%s%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME'],
       NULL
    );
});

$check_sidebar = new \Twig\TwigFunction('checkSidebar', function($prefix){
        $baseUrl = base_url();
        $currenUrl = $baseUrl.$_SERVER['REQUEST_URI'];

        $sanitize = str_replace($baseUrl, '', $currenUrl);

        if (strpos($sanitize, $prefix) > 0) {
            return true;
        }

        return false;
});

$count = new \Twig\TwigFunction('count', function($data){
    if (is_array($data)) {
        return count($data);
    }

    return false;
});

$status_simpanan = new \Twig\TwigFunction('checkStatus', function($status){
    if ($status == 1){
        echo "<span class='badge badge-success'>Aktif</span>";
    } else {
        echo "<span class='badge badge-danger'>Tidak Aktif</span>";
    }
});

$convert_number = new \Twig\TwigFunction('convertNumber', function($number){
    return "Rp. " . number_format($number);
});

$get_jenis = new \Twig\TwigFunction('getJenis', function($status){
    if ($status == 1){
        return "Setoran";
    } else {
        return "Penarikan";
    }
});

$get_due = new \Twig\TwigFunction('getDueDate', function($date, $long) {
   $dt = new Carbon\Carbon($date);
   
   return $dt->addYears($long)->format('Y-m-d');
});

$get_status = new \Twig\TwigFunction('getStatusPinjaman', function(int $status) {
    return ($status === 0) ? '<span class="badge badge-danger">Dalam Proses</span>':'<span class="badge badge-success">Lunas</span>';
});


$twig = new \Twig\Environment($loader, [
    'debug' => true,
    // ...
]);

$twig->addExtension(new \Twig\Extension\DebugExtension());

$twig->addFunction($get_flash);
$twig->addFunction($check_auth);
$twig->addFunction($get_auth_user);
$twig->addFunction($get_user_type);
$twig->addFunction($check_flash);
$twig->addFunction($get_jk);
$twig->addFunction($base_url);
$twig->addFunction($check_sidebar);
$twig->addFunction($count);
$twig->addFunction($status_simpanan);
$twig->addFunction($convert_number);
$twig->addFunction($get_jenis);
$twig->addFunction($get_due);
$twig->addFUnction($get_status);
