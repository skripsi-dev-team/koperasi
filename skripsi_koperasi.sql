-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2019 at 01:34 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi_koperasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `angsuran`
--

CREATE TABLE `angsuran` (
  `id` int(11) NOT NULL,
  `kode_angsuran` varchar(50) NOT NULL,
  `tanggal_angsur` date NOT NULL,
  `jumlah` int(11) NOT NULL,
  `bunga` float NOT NULL,
  `keterangan` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `id_pinjaman` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `angsuran`
--

INSERT INTO `angsuran` (`id`, `kode_angsuran`, `tanggal_angsur`, `jumlah`, `bunga`, `keterangan`, `user_id`, `id_pinjaman`) VALUES
(2, '1567864506_2', '2019-09-07', 1191600, 8400, 'angsuran', 1, 2),
(3, '1567864775_2', '2019-09-07', 496500, 3500, 'angsuran', 1, 2),
(4, '1568115417_5', '2019-09-10', 10000, 0, 'bayar', 2, 5),
(5, '1568116789_4', '2019-09-10', 495000, 5000, 'bayar', 2, 4),
(8, '1568117707_4', '2019-09-10', 297475, 2525, 'bayar', 2, 4),
(9, '1571675264_4', '2019-10-21', 42737, 7263.38, 'ya', 2, 4),
(10, '1571732303_2', '2019-10-22', -349050, 349100, 'l', 2, 2),
(11, '1571732339_2', '2019-10-22', -263760, 363760, 'bayar', 2, 2),
(12, '1571732356_2', '2019-10-22', 125162, 374838, 'bayar', 2, 2),
(13, '1571732759_6', '2019-10-22', 9000, 1000, 'bayar', 2, 6),
(14, '1571733209_6', '2019-10-22', 9090, 910, 'bayar', 2, 6),
(15, '1571733261_6', '2019-10-22', 9181, 819.1, 'bayar', 2, 6),
(16, '1571733278_6', '2019-10-22', 69273, 727.29, 'bayar', 2, 6),
(17, '1571733309_6', '2019-10-22', 2965, 34.56, 'bayar', 2, 6),
(18, '1571733326_6', '2019-10-22', 491, 0, 'lunas', 2, 6),
(19, '1571733364_3', '2019-10-22', -2000000, 2100000, 'hutang', 2, 3),
(20, '1571733386_3', '2019-10-22', 816000, 2184000, 'hutang', 2, 3),
(21, '1571733404_3', '2019-10-22', -149728, 2149730, 'hutang', 2, 3),
(22, '1571733714_7', '2019-10-22', 8500, 1500, 'bayar', 2, 7);

-- --------------------------------------------------------

--
-- Table structure for table `mutasi`
--

CREATE TABLE `mutasi` (
  `id` int(11) NOT NULL,
  `kode_simpanan` varchar(25) NOT NULL,
  `jenis_mutasi` tinyint(1) NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mutasi`
--

INSERT INTO `mutasi` (`id`, `kode_simpanan`, `jenis_mutasi`, `tanggal`, `jumlah`, `keterangan`, `user_id`) VALUES
(1, '15675116018', 1, '2019-09-10', 50000, 'setor', 2),
(2, '15681142905', 1, '2019-09-10', 500000, 'Setoran awal', 2);

-- --------------------------------------------------------

--
-- Table structure for table `nasabah`
--

CREATE TABLE `nasabah` (
  `id` int(11) NOT NULL,
  `nama_nasabah` varchar(191) NOT NULL,
  `tempat_lahir` varchar(191) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('l','p') NOT NULL,
  `pekerjaan` varchar(191) NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nasabah`
--

INSERT INTO `nasabah` (`id`, `nama_nasabah`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `pekerjaan`, `alamat`, `no_telp`, `email`, `password`) VALUES
(1, 'Surya', 'Singaraja', '2014-11-18', 'p', 'Programer', 'Jl tukad pancoran gang jagung no 32', '089605622619', 'suryaherdiyanto@gmail.com', '4297f44b13955235245b2497399d7a93'),
(34, 'Safirudin', 'Singaraja', '2019-09-02', 'l', 'PNS', 'asdf', '081293887', 'safir@gmail.com', '4297f44b13955235245b2497399d7a93'),
(39, 'I Wayan Kota', 'Singaraja', '2019-09-11', 'l', 'PNS', 'Panjer', '08129387623', 'kota@gmail.com', '4297f44b13955235245b2497399d7a93'),
(40, 'Mohammed Salah', 'Egypt', '2019-09-01', 'l', 'Footballer', 'Liverpool FC', '08208202', 'mosalah@lfc.com', 'efe6398127928f1b2e9ef3207fb82663'),
(41, 'Virgil Van Dijk', 'Netherland', '1987-10-20', 'l', 'Footballer', 'Liverpool', '082802802', 'virgil@lfc.com', 'efe6398127928f1b2e9ef3207fb82663'),
(42, 'Cristiano Ronaldo', 'Portugal', '1986-10-22', 'l', 'Footballer', 'Juventus', '0820820', 'cristiano@cr7.com', 'efe6398127928f1b2e9ef3207fb82663');

-- --------------------------------------------------------

--
-- Table structure for table `pinjaman`
--

CREATE TABLE `pinjaman` (
  `id_pinjaman` int(11) NOT NULL,
  `kode_pinjaman` varchar(20) NOT NULL,
  `nasabah_id` int(11) NOT NULL,
  `tanggal_pinjaman` date NOT NULL,
  `jumlah_pinjam` int(11) NOT NULL,
  `jangka_waktu` int(11) NOT NULL,
  `jaminan` varchar(191) NOT NULL,
  `tanggal_jatuh_tempo` int(11) NOT NULL,
  `status_pinjaman` varchar(5) NOT NULL,
  `bunga` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pinjaman`
--

INSERT INTO `pinjaman` (`id_pinjaman`, `kode_pinjaman`, `nasabah_id`, `tanggal_pinjaman`, `jumlah_pinjam`, `jangka_waktu`, `jaminan`, `tanggal_jatuh_tempo`, `status_pinjaman`, `bunga`) VALUES
(2, '1567773412', 1, '2019-09-06', 10000000, 5, 'BPKB', 15, '0', 0.7),
(3, '1567773448', 39, '2019-09-06', 50000000, 10, 'Sertifikan Tanah', 15, '0', 0.7),
(4, '1568114509', 40, '2019-09-10', 1000000, 1, 'BPKB', 15, '0', 0.5),
(5, '1568115402', 34, '2019-09-10', 10000, 1, 'mobil', 10, '1', 1),
(6, '1571732619', 41, '2019-10-22', 100000, 3, 'BPKB', 22, '1', 1),
(7, '1571733660', 42, '2019-10-22', 100000, 2, 'RUmah', 21, '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `simpanan`
--

CREATE TABLE `simpanan` (
  `id` int(11) NOT NULL,
  `kode_simpanan` varchar(20) NOT NULL,
  `nasabah_id` int(11) NOT NULL,
  `total_simpanan` int(11) NOT NULL,
  `status_simpanan` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `simpanan`
--

INSERT INTO `simpanan` (`id`, `kode_simpanan`, `nasabah_id`, `total_simpanan`, `status_simpanan`) VALUES
(1, '15675116018', 1, 50000, '1'),
(2, '156751218614', 38, 0, '1'),
(3, '15676021544', 39, 0, '1'),
(4, '15681142905', 40, 500000, '1'),
(5, '15717325865', 41, 0, '1'),
(6, '15717335546', 42, 0, '1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_user` varchar(191) NOT NULL,
  `tempat_lahir` varchar(191) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('l','p') NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `nama_user`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `no_telp`) VALUES
(1, 'suryaherdiyanto@gmail.com', '4297f44b13955235245b2497399d7a93', 'surya', 'singaraja', '2019-08-07', 'l', 'asdf', '12312123'),
(2, 'refo@dev.com', 'efe6398127928f1b2e9ef3207fb82663', 'Refo Junior', 'Denpasar', '2019-09-10', 'l', 'Jl. Gn. Andakasa', '08970976322');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `angsuran`
--
ALTER TABLE `angsuran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mutasi`
--
ALTER TABLE `mutasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nasabah`
--
ALTER TABLE `nasabah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pinjaman`
--
ALTER TABLE `pinjaman`
  ADD PRIMARY KEY (`id_pinjaman`);

--
-- Indexes for table `simpanan`
--
ALTER TABLE `simpanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `angsuran`
--
ALTER TABLE `angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `mutasi`
--
ALTER TABLE `mutasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `nasabah`
--
ALTER TABLE `nasabah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `pinjaman`
--
ALTER TABLE `pinjaman`
  MODIFY `id_pinjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `simpanan`
--
ALTER TABLE `simpanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
